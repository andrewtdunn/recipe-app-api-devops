resource "aws_s3_bucket" "app_public_files" {
  bucket        = "atd2005-${local.prefix}-files"
  acl           = "public-read"
  force_destroy = true
}
